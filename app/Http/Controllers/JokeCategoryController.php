<?php

namespace App\Http\Controllers;

use App\Joke;
use App\JokeCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JokeCategoryController extends Controller
{
    public function index($categorySlug, Request $request) {
        $category = JokeCategory::where('slug', $categorySlug)->orderBy('id', 'desc')->first();

        $jokes = Joke::where('joke_category_id', $category->id)->orderBy(DB::raw('RAND('.date('d').')'))->paginate();

        if($category!==null) {
            return view('joke_category.index', ['category' => $category, 'jokes'=>$jokes]);
        }

        return abort(404);
    }

    public function getRenderedPage($categorySlug, $page=1) {
        $html = '';

        $category = JokeCategory::where('slug', $categorySlug)->first();
        if($category!==null) {
            $jokes = Joke::where('joke_category_id', $category->id)->orderBy(DB::raw('RAND('.date('d').')'))->paginate(15, ['*'], 'page', $page);

            if ($jokes !== null) {
                foreach ($jokes as $joke) {
                    try {
                        $html .= view('includes.joke.jokeTypeSelector', ['joke' => $joke])->render();
                    } catch (\Exception $e) {

                    } catch (\Throwable $e) {

                    }
                }
            }
        }
        return response()->json($html);
    }
}
