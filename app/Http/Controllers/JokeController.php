<?php

namespace App\Http\Controllers;

use App\Joke;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JokeController extends Controller
{
    public function index($jokeSlug, Request $request) {
        $joke = Joke::where('slug', $jokeSlug)->first();
        if($joke!==null) {
            return view('joke.index', ['joke'=>$joke]);
        }

        return abort(404);
    }

    public function type($type, Request $request) {
        if($type=='szoveg') {
            $jokes = Joke::where('type', 'text')->orderBy(DB::raw('RAND('.date('d').')'))->paginate();
            return view('joke.type', ['jokes'=>$jokes, 'title'=>'Vicces szövegek', 'type'=>$type]);
        }elseif ($type=='kep'){
            $jokes = Joke::where('type', 'image')->orderBy(DB::raw('RAND('.date('d').')'))->paginate();
            return view('joke.type', ['jokes'=>$jokes, 'title'=>'Vicces képek', 'type'=>$type]);
        }elseif ($type=='video'){
            $jokes = Joke::where('type', 'video')->orderBy(DB::raw('RAND('.date('d').')'))->paginate();
            return view('joke.type', ['jokes'=>$jokes, 'title'=>'Vicces videók', 'type'=>$type]);
        }

        return abort(404);
    }

    public function getRenderedPage($type, $page=1) {
        $html = '';

        if($type=='szoveg') $type = 'text';
        if($type=='kep') $type = 'image';
        if($type=='video') $type = 'video';

        $jokes = Joke::where('type', $type)->orderBy(DB::raw('RAND('.date('d').')'))->paginate(15, ['*'], 'page', $page);

        if($jokes!==null) {
            foreach ($jokes as $joke) {
                try {
                    $html .= view('includes.joke.jokeTypeSelector', ['joke' => $joke])->render();
                }catch (\Exception $e) {

                }catch (\Throwable $e){

                }
            }
        }

        return response()->json($html);
    }
}
