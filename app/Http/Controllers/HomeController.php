<?php

namespace App\Http\Controllers;

use App\Joke;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(Request $request) {
        if($request->has('s')) {
            $term = $request->get('s');
            if($term!==null) {
                $jokes = Joke::where('content', 'like', '%'.$term.'%')->orWhere('title', 'like', '%'.$term.'%')->orderBy(DB::raw('RAND('.date('d').')'))->paginate();
                return view('search', ['jokes' => $jokes, 'term'=>$term]);
            }else{
                return abort(404);
            }
        }else {
            $jokes = Joke::orderBy(DB::raw('RAND('.date('d').')'))->paginate();
            return view('homepage', ['jokes' => $jokes]);
        }
    }

    public function getRenderedSearchPage($term, $page=2) {
        $html = '';
        $jokes = Joke::where('content', 'like', '%'.$term.'%')->orWhere('title', 'like', '%'.$term.'%')->orderBy(DB::raw('RAND('.date('d').')'))->paginate(15, ['*'], 'page', $page);

        if($jokes!==null) {
            foreach ($jokes as $joke) {
                try {
                    $html .= view('includes.joke.jokeTypeSelector', ['joke' => $joke])->render();
                }catch (\Exception $e) {

                }catch (\Throwable $e){

                }
            }
        }

        return response()->json($html);
    }

    public function getRenderedPage($page=1) {
        $html = '';

        $jokes = Joke::orderBy(DB::raw('RAND('.date('d').')'))->paginate(15, ['*'], 'page', $page);

        if($jokes!==null) {
            foreach ($jokes as $joke) {
                try {
                    $html .= view('includes.joke.jokeTypeSelector', ['joke' => $joke])->render();
                }catch (\Exception $e) {

                }catch (\Throwable $e){

                }
            }
        }

        return response()->json($html);
    }
}
