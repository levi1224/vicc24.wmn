<?php

namespace App\Http\Controllers;

use App\Joke;
use App\JokeCategory;
use App\SitemapGenerator;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    public static function generate() {
        $sitemapController = new SitemapController();
        $sitemapController->index();
    }

    public function index() {
        $sitemapIndex = new SitemapGenerator('sitemap.xml', true);
        $urls = $this->categories();
        foreach ($urls as $url) {
            $sitemapIndex->addSitemapPage(url($url));
        }

        $urls = $this->jokes();
        foreach ($urls as $url) {
            $sitemapIndex->addSitemapPage(url($url));
        }

        $sitemapIndex->export();
    }

    private function categories() {
        $names = [];
        $categories = JokeCategory::orderBy('id', 'asc')->get();
        $numberOfPages = 0;

        foreach ($categories->chunk(500) as $chunkedCategories) {
            $numberOfPages++;
            $sitemapUrl = 'sitemap_category_'.$numberOfPages.'.xml';
            $names[] = $sitemapUrl;
            $sitemap = new SitemapGenerator($sitemapUrl);
            foreach ($chunkedCategories as $oneChunkedCategory) {
                $sitemap->addPage(route('category', ['categorySlug'=>$oneChunkedCategory->slug]));
            }
            $sitemap->export();
            unset($sitemap);
        }

        return $names;
    }

    private function jokes() {
        $names = [];
        $jokes = Joke::orderBy('id', 'asc')->get();
        $numberOfPages = 0;

        foreach ($jokes->chunk(500) as $chunkedJokes) {
            $numberOfPages++;
            $sitemapUrl = 'sitemap_joke_'.$numberOfPages.'.xml';
            $names[] = $sitemapUrl;
            $sitemap = new SitemapGenerator($sitemapUrl);
            foreach ($chunkedJokes as $oneChunkedJoke) {
                $sitemap->addPage(route('joke', ['jokeSlug'=>$oneChunkedJoke->slug]));
            }
            $sitemap->export();
            unset($sitemap);
        }

        return $names;
    }
}
