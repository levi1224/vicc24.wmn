<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function termOfUse() {
        return view('pages.termOfUse');
    }

    public function privacyPolicy() {
        return view('pages.privacyPolicy');
    }

    public function contact() {
        return view('pages.contact');
    }
}
