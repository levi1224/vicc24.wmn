<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SitemapGenerator extends Model
{
    private $xml;
    private $name;

    public function __construct($name, $isIndex=false, array $attributes = []) {
        parent::__construct($attributes);
        if($isIndex) {
            $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></sitemapindex>');
        }else{
            $this->xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"></urlset>');
        }
        $this->name = $name;
    }

    public function addSitemapPage($url) {
        $this->xml->addChild('sitemap')->addChild('loc', $url);
    }

    public function addPage($url) {
        $this->xml->addChild('url')->addChild('loc', $url);
    }

    public function export() {
        $this->xml->saveXML(public_path().'/'.$this->name);
    }
}
