<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Vote
 * @property int $id
 * @property $user_id
 * @property $joke_id
 * @property User $user
 * @property Joke $joke
 * @package App
 */
class Vote extends Model
{
    //
}
