<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class JokeCategory
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $long_description
 * @property int|null $joke_category_id
 * @property JokeCategory $parent_category
 * @package App
 */
class JokeCategory extends Model
{
    public $table = 'joke_categories';
    public $timestamps = null;

    /**
     * @param string $name Name of the category
     * @param string $slug Slug for the category
     * @param int|null $parent_category_id Parent id from joke category
     * @param string|null $description Category SEO description
     * @param string|null $long_description Category description. This show on category page
     * @return int|null
     */
    public static function insertCategoryAndGetId($name, $slug, $parent_category_id, $description=null, $long_description=null) {
        if(!self::existsByNameSlugPCategory($name, $slug, $parent_category_id)) {
            $new = new self();
            $new->name = $name;
            $new->slug = $slug;
            $new->joke_category_id = $parent_category_id;
            $new->description = $description;
            $new->long_description = $long_description;
            $new->save();
            return $new->id;
        }else{
            return self::getId($name, $slug, $parent_category_id);
        }
    }

    /**
     * @param $name
     * @param $slug
     * @param $parent_category_id
     * @return bool
     */
    public static function existsByNameSlugPCategory($name, $slug, $parent_category_id) {
        return self::getId($name, $slug, $parent_category_id)!==null;
    }

    /**
     * @param $name
     * @param $slug
     * @param $parent_category_id
     * @return int|null
     */
    public static function getId($name, $slug, $parent_category_id) {
        $temp = self::where('name', $name)->where('slug', $slug)->where('joke_category_id', $parent_category_id)
            ->first();
        if($temp!==null) {
            return $temp->id;
        }

        return null;
    }

    /**
     * @param $id
     * @return JokeCategory|null
     */
    public static function getById($id) {
        return self::where('id', $id)->first();
    }

    public function jokes() {
        return Joke::where('joke_category_id', $this->id)->get();
    }
}
