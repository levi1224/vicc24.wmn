<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Goutte\Client;

class Uploader extends Model
{
    private $jokes;
    private $category;
    private $linkek;

    public function getJokePage() {
        $from = DB::table('automated')->where('name', '=', 'from')->first()->value;
        $categoryUrlId = DB::table('automated')->where('name', '=', 'categoryUrlId')->first()->value;

        if($from!=-1 && $categoryUrlId!=-1) {
            $categoryUrl = $this->getCategoryLinkById($categoryUrlId);
            if ($categoryUrl !== null) {
                $this->getJokes($categoryUrl . '&honnan=' . $from);
                if (count($this->jokes) > 0) {
                    foreach ($this->jokes as $joke) {
                        $this->insertJoke($joke);
                    }
                }
                if (count($this->jokes) < 10) {
                    DB::table('automated')->where('name', '=', 'categoryUrlId')->update(['value' => ($categoryUrlId + 1)]);
                    DB::table('automated')->where('name', '=', 'from')->update(['value' => '0']);
                } else {
                    DB::table('automated')->where('name', '=', 'from')->update(['value' => ($from + 10)]);
                }
            } else {
                DB::table('automated')->where('name', '=', 'categoryUrlId')->update(['value' => '-1']);
                DB::table('automated')->where('name', '=', 'from')->update(['value' => '-1']);
            }
        }
    }

    public function insertJoke($joke) {
        $exists = DB::table('upload_jokes')->where('title', '=', $joke['title'])->where('category', '=', $joke['category'])->count();
        if($exists==0) {
            try{
                DB::table('upload_jokes')->insert([
                    'title' => $joke['title'],
                    'content' => $joke['content'],
                    'category' => $joke['category']
                ]);
            }catch(\Exception $e) {

            }
        }
    }

    public function getJokes($url) {
        $this->jokes = array();

        $client = new Client();
        $crawler = $client->request('GET', $url);
        $this->category = $crawler->filter('h1')->text();
        $crawler->filter('table[style="BACKGROUND: white; FONT-SIZE: 18px; BORDER: Black 1px Solid;"]')->each(function ($jokeBox) {
            $joke = array();
            try {
                $joke['title'] = $jokeBox->filter('tr[class="nagytabla_fejlec"]')->first()->filter('td')->first()->filter('a')->first()->html();
                $joke['content'] = strip_tags(self::cleanJoke(nl2br($jokeBox->filter('tr')->getNode(1)->textContent)), '<br>');
                $joke['category'] = $this->category;
                $this->jokes[] = $joke;
            } catch (\Exception $e) {

            }
        });
    }

    public static function cleanJoke($text) {
        $to = strpos($text, 'Szerinted hány pontos volt ez a vicc?');
        return trim(str_replace(PHP_EOL, '', substr($text, 0, $to)));
    }

    public function getCategoryLinkById($id) {
        $this->linkek = array();

        $client = new Client();
        $crawler = $client->request('GET', 'https://www.viccesviccek.hu');
        $crawler->filter('a[class="narancsmenu_uj"]')->each(function ($link) {
            $this->linkek[] = 'https://www.viccesviccek.hu/'.$link->attr('href');
        });

        if($id>0 && $id<(count($this->linkek))) {
            return $this->linkek[$id];
        }else{
            return null;
        }
    }

    public function uploadJoke() {
        $joke = DB::table('upload_jokes')->where('uploaded', '=', '0')->limit(1)->first();
        if($joke!==null) {
            $joke->content = strip_tags($joke->content, '<br>');
            $joke->content = str_replace('(adsbygoogle = window.adsbygoogle || []).push({});', '', trim($joke->content));

            if(strpos($joke->content, '<br />')===0) $joke->content = trim(substr($joke->content, 6));
            if(strpos($joke->content, '<br />')===0) $joke->content = trim(substr($joke->content, 6));
            if(strpos($joke->content, '<br />')===0) $joke->content = trim(substr($joke->content, 6));

            DB::table('upload_jokes')->where('id', '=', $joke->id)->update(['uploaded' => 2]);
            $seoDesc = 'Ki ne ismerne '. mb_convert_case($joke->category, MB_CASE_LOWER, 'UTF-8').'et? Ha a válaszod "én" volt, akkor kattints ide, mert mi rengeteget ismerünk!';
            $insertedCategoryId = JokeCategory::insertCategoryAndGetId($joke->category, str_slug($joke->category), null, $seoDesc, $seoDesc);
            $insertedJokeId = Joke::insertAndGetId($joke->title, $joke->content, str_slug($joke->title), 'text', $insertedCategoryId, $joke->content);

            DB::table('upload_jokes')->where('id', '=', $joke->id)->update(['uploaded' => 1]);
        }
    }
}
