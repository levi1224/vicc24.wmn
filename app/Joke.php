<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Joke
 * @property int $id
 * @property JokeCategory $category
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $slug
 * @property string $type
 * @property Vote $votes
 * @property int $voteCount
 * @property float $voteAvg
 * @package App
 */
class Joke extends Model
{
    public $voteCount;
    public $voteAvg;

    /**
     * @param string $title
     * @param string $description
     * @param string $content
     * @param string $slug
     * @param string $type
     * @param int $joke_category_id
     * @return int|null
     */
    public static function insertAndGetId($title, $content, $slug, $type, $joke_category_id, $description=null) {
        if($description==null) {
            $description = $content;
        }

        if(!self::exists($title, $description, $content, $slug, $type)) {
            $new = new Joke();
            $new->title = $title;
            $new->joke_category_id = $joke_category_id;
            $new->description = mb_substr($description, 0, 185, 'UTF-8');
            $new->content = $content;
            $new->slug = $slug;
            $new->type = $type;
            $new->save();
            return $new->id;
        }else{
            return self::getId($title, $description, $content, $slug, $type);
        }
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $content
     * @param string $slug
     * @param string $type
     * @return bool
     */
    public static function exists($title, $description, $content, $slug, $type) {
        return self::getId($title, $description, $content, $slug, $type)!==null;
    }

    /**
     * @param string $title
     * @param string $description
     * @param string $content
     * @param string $slug
     * @param string $type
     * @return int|null
     */
    public static function getId($title, $description, $content, $slug, $type) {
        $temp = self::where('title', $title)->where('description', $description)
            ->where('content', $content)->where('slug', $slug)->where('type', $type)->first();
        if($temp!==null) {
            return $temp->id;
        }

        return null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category() {
        return $this->belongsTo('App\JokeCategory', 'joke_category_id', 'id');
    }
}
