<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJokeCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('joke_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('joke_category_id')->nullable();
            $table->string('name', 190);
            $table->string('slug', 190);
            $table->string('description', 190)->nullable();
            $table->string('long_description', 1000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('joke_categories');
    }
}
