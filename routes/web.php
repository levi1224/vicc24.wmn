<?php

// Homepage
Route::get('/', 'HomeController@index')->name('homepage');

// Register URLs
Route::get('/regisztracio/facebook', 'SocialiteController@registerWithFacebook')->name('registerWithFacebook');
Route::get('/regisztracio/google', 'SocialiteController@registerWithGoogle')->name('registerWithGoogle');

// Login URLs
Route::get('/bejelentkezes/facebook', 'SocialiteController@loginWithFacebook')->name('loginWithFacebook');
Route::get('/bejelentkezes/google', 'SocialiteController@loginWithGoogle')->name('loginWithGoogle');

// Pages
Route::get('/oldalak/felhasznalasi-feltetelek', 'PageController@termOfUse')->name('termOfUsePage');
Route::get('/oldalak/adatkezeles', 'PageController@privacyPolicy')->name('privacyPolicyPage');
Route::get('/oldalak/kapcsolat', 'PageController@contact')->name('contactPage');

// JokeCategory page
Route::get('/kategoria/{categorySlug}', 'JokeCategoryController@index')->name('category');

// Type page
Route::get('/tipus/{type}', 'JokeController@type')->name('type');

// Joke page
Route::get('/vicc/{jokeSlug}', 'JokeController@index')->name('joke');

// Admin routes
Route::get('admin/sitemap/generate', 'AdminController@sitemapGenerate')->name('sitemapGenerate');