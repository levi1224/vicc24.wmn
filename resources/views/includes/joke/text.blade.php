<div class="card mb-4">
    <div class="card-body">
        <h2 class="card-title">{{ $joke->title }}</h2>
        <p class="card-text">{!! $joke->content !!}</p>
    </div>
    <div class="card-footer text-muted">
        <a href="{{ route('type', ['type'=>'szoveg']) }}"><span class="badge badge-primary">vicces szövegek</span></a>
        <a href="{{ route('category', ['categorySlug'=>$joke->category->slug]) }}"><span class="badge badge-warning">{{ $joke->category->name }}</span></a>
    </div>
    <div class="card-footer text-muted text-center">
        <a href="{{ route('joke', ['jokeSlug'=>$joke->slug]) }}" class="btn btn-primary">Megnézem</a>
        <div class="share-box">
            Megosztás
            <br/>
            <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(route('joke', ['jokeSlug'=>$joke->slug])) }}"><i class="fab fa-facebook-square fa-2x"></i></a>
            <a href="https://plus.google.com/share?url={{ urlencode(route('joke', ['jokeSlug'=>$joke->slug])) }}"><i class="fab fa-google-plus-square fa-2x"></i></a>
            <a href="https://twitter.com/intent/tweet?url={{ urlencode(route('joke', ['jokeSlug'=>$joke->slug])) }}"><i class="fab fa-twitter-square fa-2x"></i></a>
        </div>
    </div>
</div>