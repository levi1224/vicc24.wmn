@hasSection('title')
<title>@yield('title') - {{ env('APP_NAME') }}</title>
@else
<title>{{ env('APP_NAME') }} - {{ env('SEO_KEYWORDS') }}</title>
@endif
@if(isset($metaRobots) && $metaRobots==true)
<meta name="robots" content="noindex, nofollow">
@endif
<meta name="description" content="@yield('description', env('SEO_DESCRIPTION'))" />
@hasSection('canonical')
<link rel="canonical" href="@yield('canonical')" />
@endif
<meta property="og:locale" content="hu_HU" />
<meta property="og:type" content="website" />
@hasSection('title')
<meta property="og:title" content="@yield('title') - {{ env('APP_NAME') }}" />
<meta name="twitter:title" content="@yield('title') - {{ env('APP_NAME') }}" />
@else
<meta property="og:title" content="{{ env('APP_NAME') }} - {{ env('SEO_KEYWORDS') }}" />
<meta name="twitter:title" content="{{ env('APP_NAME') }} - {{ env('SEO_KEYWORDS') }}" />
@endif
<meta property="og:description" content="@yield('description', env('SEO_DESCRIPTION'))" />
<meta name="twitter:description" content="@yield('description', env('SEO_DESCRIPTION'))" />
<meta property="og:url" content="{{ \Illuminate\Support\Facades\Request::url() }}" />
<meta property="og:site_name" content="{{ env('APP_NAME') }}" />
<meta name="twitter:card" content="summary_large_image" />
<script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "WebSite",
  "name": "{{ env('APP_NAME') }}",
  "url": "{{ \Illuminate\Support\Facades\Request::root() }}",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "{{ \Illuminate\Support\Facades\Request::root() }}?s={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
</script>
<meta name="author" content="https://plus.google.com/109215061273219373439">
