@extends('layouts.app')

@section('content')
    <h1>A tartalom nem létezik</h1>
    <p>Olyan területre tévedtél, ahol még ember nem járt ezen az oldalon!</p>
    <p>A főoldalra <a href="{{ url('/') }}">ide</a> kattintva jutsz el.</p>
@endSection