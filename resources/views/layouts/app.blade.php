<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @include('includes.seo.basic')
    @if(!env('APP_DEBUG'))
    @include('includes.google.analytics')
    @include('includes.google.tagmanager')
    @endif
    @include('includes.google.adsense.autoads')
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
</head>
<body>
@include('includes.google.tagmanagerbody')
@include('includes.facebook.basic')
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        @if(Request::is('/'))
            <h1 class="site-title">
                <a href="{{ url('/') }}">
                    <img class="page-logo" src="{{ asset('img/page/logo.png') }}" alt="{{ env('APP_NAME') }} - {{ env('SEO_KEYWORDS') }}" title="{{ env('APP_NAME') }} - {{ env('SEO_KEYWORDS') }}">
                </a>
            </h1>
        @else
            <div class="site-title">
                <a href="{{ url('/') }}">
                    <img class="page-logo" src="{{ asset('img/page/logo.png') }}" alt="{{ env('APP_NAME') }} - {{ env('SEO_KEYWORDS') }}" title="{{ env('APP_NAME') }} - {{ env('SEO_KEYWORDS') }}">
                </a>
            </div>
        @endif
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Kezdőlap</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('type', ['type'=>'szoveg']) }}">Vicces szövegek</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('type', ['type'=>'kep']) }}">Vicces képek</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('type', ['type'=>'video']) }}">Vicces videók</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contactPage') }}">Kapcsolat</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-8 order-md-1 order-2">
            @hasSection('content')
                @yield('content')
            @endif
            @include('includes.google.adsense.adaptive')
        </div>
        <div class="col-md-4 order-md-2 order-1">
            <div class="card my-4">
                <form action="{{ url('/') }}" method="GET">
                    <h5 class="card-header">Keresés</h5>
                    <div class="card-body">
                        <div class="input-group">
                            <input type="text" name="s" class="form-control" placeholder="móricka">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary" type="button">Keresés</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="text-center">
                @include('includes.facebook.pageplugin')
            </div>
            <div class="card my-4">
                <h5 class="card-header">Kategóriák</h5>
                <div class="card-body">
                    <div class="row">
                        @foreach($categories->chunk(round(count($categories)/2, 0, PHP_ROUND_HALF_UP)) as $chunkedCategories)
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                @foreach($chunkedCategories as $category)
                                <li>
                                    <a href="{{ route('category', ['categorySlug'=>$category->slug]) }}">{{ $category->name }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="card my-4">
                <h5 class="card-header">Vicc típusok</h5>
                <div class="card-body">
                    <ul class="list-unstyled mb-0">
                        <li>
                            <a href="{{ route('type', ['type'=>'szoveg']) }}">Vicces szövegek</a>
                        </li>
                        <li>
                            <a href="{{ route('type', ['type'=>'kep']) }}">Vicces képek</a>
                        </li>
                        <li>
                            <a href="{{ route('type', ['type'=>'video']) }}">Vicces videók</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div>
                @include('includes.google.adsense.sidebar')
            </div>
        </div>
    </div>
</div>
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; <a href="{{ url('/') }}">{{ env('APP_NAME') }}</a> - @yield('seo_keywords', env('SEO_KEYWORDS'))</p>
    </div>
</footer>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
@stack('javascript')
</body>
</html>
