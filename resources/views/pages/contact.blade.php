@extends('layouts.app')

@section('title', 'Kapcsolat')
@section('description', 'Ha kapcsolatba szeretnél lépni velünk, ezen az oldalon megtalálod a szükséges információkat!')

@section('content')
    <h1>Kapcsolat</h1>
    <p>Ha fel szeretnéd venni velünk a kapcsolatot, írj facebook-on.</p>
    <p>Ha más formában keresnél fel, írj a levente.software[kukac]gmail[pont]com e-mail címre.</p>
@endSection