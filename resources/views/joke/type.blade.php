@extends('layouts.app')

@section('title', $title)
@section('seo_keywords', $title)
@section('description', $title.' nálunk tömegszámra vannak! Ha ezt kerested, kattints ide és garantáltan itt ragadsz a poénok és a jókedv társaságában!')

@section('content')
    <h1>{{ $title }}</h1>
    <div id="jokes-box">
    @if($jokes!==null)
        @foreach($jokes as $joke)
            @include('includes.joke.jokeTypeSelector', ['joke'=>$joke])
        @endforeach
    @endif
    </div>
    <div id="moreLink" class="text-center">
        <button id="moreButton" class="btn btn-danger">Több viccet akarok!</button>
    </div>
@endSection

@push('javascript')
    <script>
        var page = 2;
        var hasMore = true;

        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                getMore();
            }
        });

        $('#moreButton').click(function () {
            getMore();
        });

        function getMore() {
            if(hasMore) {
                $.get('/api/jokeType/{{ $type }}/' + page, function (data) {
                    if(data.length > 0) {
                        $('#jokes-box').append(data);
                        page++;
                    }else{
                        hasMore = false;
                        $('#moreLink').hide();
                    }
                });
            }
        }
    </script>
@endPush