@extends('layouts.app')

@section('title', $joke->title.' vicc')
@section('seo_keywords', $joke->title.' vicc')
@section('description', strip_tags($joke->description))


@section('content')
    <h1>{{ $joke->title }}</h1>
    <p>
        {!! $joke->content !!}
    </p>
    <hr>
    <div class="share-box text-center">
        Megosztás
        <br/>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(route('joke', ['jokeSlug'=>$joke->slug])) }}"><i class="fab fa-facebook-square fa-2x"></i></a>
        <a href="https://plus.google.com/share?url={{ urlencode(route('joke', ['jokeSlug'=>$joke->slug])) }}"><i class="fab fa-google-plus-square fa-2x"></i></a>
        <a href="https://twitter.com/intent/tweet?url={{ urlencode(route('joke', ['jokeSlug'=>$joke->slug])) }}"><i class="fab fa-twitter-square fa-2x"></i></a>
        <a href=""><i class="fas fa-envelope-square fa-2x"></i></a>
    </div>
    <div class="text-center">
        @include('includes.facebook.commentbox')
    </div>
@endSection