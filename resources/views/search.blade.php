@extends('layouts.app')

@section('title', $term.' keresés eredménye')
@section('seo_keywords', $term.' keresés eredménye')
@section('description', 'Keress a vicceink között! Itt találod a '.$term.' kifejezéshez tartozó találatokat! Ha téged is érdekelnek a '.$term.' témájú viccek, itt a helyed!')

@section('content')
    <h1>{{ $term }} keresés eredménye</h1>
    <div id="jokes-box">
        @if($jokes!==null)
            @foreach($jokes as $joke)
                @include('includes.joke.jokeTypeSelector', ['joke'=>$joke])
            @endforeach
        @endif
    </div>
    <div id="moreLink" class="text-center">
        <button id="moreButton" class="btn btn-danger">Több viccet akarok!</button>
    </div>
@endSection

@push('javascript')
    <script>
        var page = 2;
        var hasMore = true;

        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                getMore();
            }
        });

        $('#moreButton').click(function () {
            getMore();
        });

        function getMore() {
            if(hasMore) {
                $.get('/api/search/{{ urlencode($term) }}/' + page, function (data) {
                    if(data.length > 0) {
                        $('#jokes-box').append(data);
                        page++;
                    }else{
                        hasMore = false;
                        $('#moreLink').hide();
                    }
                });
            }
        }
    </script>
@endPush